describe('Molecule description consultation feature', () => {

	beforeEach(() => {
		cy.visit('/')

		cy.get('#id_typeQuery').select('Formula')
		cy.get('#query').type('C_')
		cy.get('#submit_search').click()

		cy.get('#query').click

		cy.get('ul#display_result')
			.find('li.list-group-item')
			.should('have.length.gt', 0)

		// Act
		cy.get('ul#display_result')
			.find('li.list-group-item')
			.first()
			.click()
	})

	it('should display the molecule description', () => {
		// Assert
		cy.url()
			.should('include', 'page_detail')
	})

	it('should by default be displaying the molecule descrition', () => {
		cy.get('#description-tab')
			.should('have.class', 'active')
	})

	it('should be possible to switch to see the result instead ', () => {
		cy.get('#results-tab').click()

		cy.get('#results-tab')
			.should('have.class', 'active')
	})

	it('should by default be displaying the sub-result option WaveFunction', () => {
		cy.get('#results-tab').click()

		cy.get('#v-pills-wavefunction-tab')
			.should('have.class', 'active')
	})

	it('should be possible to navigate between the different sub-result option', () => {
		cy.get('#results-tab').click()

		cy.get('#v-pills-geometry-tab').click()

		cy.get('#v-pills-geometry-tab').click()
			.should('have.class', 'active')
	})

	it('should be possible to switch from window display to print display', () => {
		cy.wait(1000)

		cy.get('#switchDiplay').click()

		// Should test here some changes about the Dom else the tests would not be worth the wait
	})

})