describe('Molecule search result consultation feature', () => {

	beforeEach(() => {
		cy.visit('/')

		cy.get('#id_typeQuery').select('Formula')
		cy.get('#query').type('C_')
		cy.get('#submit_search').click()

		cy.get('#query').click
	})

	it('should display at least one molecule', () => {

		cy.get('ul#display_result')
			.find('li.list-group-item')
			.should('have.length.gt', 0)

	})

	/**
	 * Limit case test:
	 * 
	 * - 26 results 
	 * - 1 pagination item 
	 * - 25 li elements
	 * 
	 * - 0 element found:
	 * - 1 pagination item
	 * - 0 li element
	 */

	/** 
	 * 26 molecules found
	 * 2 pagination item found
	 * 25 li elements shown
	 * click on show_results to change number of shown elements
	 * change to 50 entries per page
	 * check that now more than 25 or at least 50 li are being shown 
	 */



})