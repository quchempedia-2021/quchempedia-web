const { isContext } = require("vm")

describe('Search feature testing', () => {

	it('finds molecules with a given formula', () => {
		// Arrange
		cy.visit('/')

		// Act
		cy.get('#id_typeQuery').select('Formula')

		cy.get('#query').type('C_')

		cy.get('#submit_search').click()


		// Assert
		cy.url()
			.should('include', 'type=formula')
			.should('include', 'q=C_')
	})

	it('finds molecules with a SMILE ID', () => {
		// Arrange
		cy.visit('/')

		// Act
		cy.get('#id_typeQuery').select('SMILES')

		cy.get('#query').type('[CH]1[C]2NN3CN=C(O3)[C@H]12')

		cy.get('#submit_search').click()


		// Assert
		cy.url()
			.should('include', 'type=smi')
			.should('include', 'q=[CH]1[C]2NN3CN=C(O3)[C@H]12')
	})

	it('finds molecules with a InChi', () => {
		// Arrange
		cy.visit('/')

		// Act
		cy.get('#id_typeQuery').select('InChi')

		cy.get('#query').type('1S/C5H5N3O/c1-3-4(1)7-8-2-6-5(3)9-8/h1,3,7H,2H2/t3-/m1/s1')

		cy.get('#submit_search').click()


		// Assert
		cy.url()
			.should('include', 'type=inchi')
			.should('include', 'q=1S/C5H5N3O/c1-3-4(1)7-8-2-6-5(3)9-8/h1,3,7H,2H2/t3-/m1/s1')
	})

})